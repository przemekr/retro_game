#include <iostream>
#include <string>
#include "geometry.h"
#include "objects.h"
#include "agg_rounded_rect.h"

const int WALK = 0;
const int FLY = 1;
const int KEY_UP = 0xf;
const int MODE_EASY = 0;
const int MODE_HARD = 1;

class WalkingGameView : public AppView
{
public:
   WalkingGameView(App& app): AppView(app),
      win(app, 162, 620, 233, 209),
      duck(win, 0.07, 1,   1.0, 0.4, 0.03, red, IMG_DUCK_E1, 2),
      ball(win, 0.06, 0.3, 2.5, 0.6, 0.9, lblue, IMG_BALL, 1),
      pause(win, 0.1, 0.3, IMG_PAUSE),
      lighthause(win, 0.685, 0.23, IMG_LIGHTH),
      platforms(win, 0.05, -0.2, IMG_PLATF),
      b1(win, 0.02, 0.2, 0.70, yellow, IMG_COIN1, IMG_COIN6-IMG_COIN1),
      time_left(0),
      fps(30)
   {
      new_game();
   }

   void new_game()
   {
      app.start_timer();
      app.score = 0;
      touchdowns = 10;
      duck.c.x = 0.5;
      duck.c.y = 0.07;
      duck.v.x = 0.0;
      duck.v.y = 0.0;
      duck.img_start = mode == MODE_EASY? IMG_DUCK_E1: IMG_DUCK_H1;

      ball.v.x = 0.0;
      ball.v.y = 0.0;
      ball.c.x = 0.6;
      ball.c.y = 0.9;
      ball.touchdowns = 0;
      key_left = false;
      key_right = false;
      select = false;
      duck_facing_right = false;

      pause.visible = false;
      lighthause.visible = true;
      platforms.visible = true;
   }

   void enter()
   {
      new_game();
      app.wait_mode(false);
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      switch (key)
      {
         case 1073741903:
         case agg::key_right:
            if (flags&KEY_UP)
            {
               key_left = false;
               key_right = false;
            }
            else
            {
               key_left = false;
               key_right = true;
            }
            break;

         case 1073741904:
         case agg::key_left:
            if (flags&KEY_UP)
            {
               key_left = false;
               key_right = false;
            }
            else 
            {
               key_left = true;
               key_right = false;
            }
            break;

         case 1073741906:
         case agg::key_up:
            break;

         case 13:
            if (not flags&KEY_UP)
            {
               select = true;
            } else {
               select = false;
            }
            break;
      }
      on_ctrl_change();
   }

   void on_ctrl_change()
   {
      if (key_right and duck.state != FLY)
      {
         duck.v.x = 0.05;
         duck.v.y = 0.02;
         duck.state = FLY;
         duck_facing_right = true;
      }

      if (key_left and duck.state != FLY)
      {
         duck.v.x = -0.05;
         duck.v.y = 0.02;
         duck.state = FLY;
         duck_facing_right = false;
      }

      if (select)
      {
         app.wait_mode(not app.wait_mode());
         pause.visible = app.wait_mode();
         app.force_redraw();
      }
      app.force_redraw();
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      key_left = false;
      key_right = false;
      select = false;
      on_ctrl_change();
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      geometry::P click(x,y);
      if (click.dist(geometry::P(135, 197)) < 68)
         key_left = true;

      if (click.dist(geometry::P(422, 197)) < 68)
         key_right = true;

      if (click.dist(geometry::P(274, 233)) < 35)
         select = true;

      on_ctrl_change();
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      rbase.copy_from(app.rbuf_img(0));
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();

      platforms.draw(ras, sl, rbase);
      lighthause.draw(ras, sl, rbase);

      duck.draw(ras, sl, rbase, duck_facing_right);
      b1.draw(ras, sl, rbase);
      ball.draw(ras, sl, rbase);

      Item d100(win, 0.50, -0.23,  IMG_DIG0+app.score/100 %10);
      Item d10 (win, 0.55, -0.23, IMG_DIG0+app.score/10  %10);
      Item d1  (win, 0.60, -0.23, IMG_DIG0+app.score     %10);

      d100.draw(ras, sl, rbase);
      d10.draw(ras, sl, rbase);
      d1.draw(ras, sl, rbase);
      

      pause.draw(ras, sl, rbase);

      if (not key_left)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_LEFT)), 0, 64, 165);

      if (not key_right)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_RIGHT)), 0, 331, 165);

      if (not select)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_SELECT)), 0, 247, 230);

      rbase.blend_from(pixfmt_type(win.app.rbuf_img(IMG_COVER)), 0, 0, 0);
   }

   void update(long elapsed_time)
   {
      double t1 = geometry::time_to_colision(duck.c, duck.v, 1, 1);
      double t2 = geometry::time_to_colision(ball.c, ball.v, 1, 1);
      double t3 = geometry::time_to_colision(duck.c, ball.c,
            duck.v, ball.v);
      double t = std::min(0.25, std::min(std::min(t1, t2), t3));
      duck.updateV(t);
      ball.updateV(t);
      duck.img_idx = 0;
      if (duck.state == WALK)
      {
         duck.v.y = 0.0;
         duck.rotat = 0.0;
         duck.rotatd = 0.0;
      }
      duck.move(t);
      ball.move(t);
      if (t == t3)
      {
         app.play_sound(1, 1500*geometry::mod(
                  duck.v.x-ball.v.x,
                  duck.v.y-ball.v.y));
         geometry::P org_v = duck.v;
         duck.v.y = 0.10;
         duck.collide(ball);
         duck.img_idx = 1;
         duck.v = org_v;
         duck.move(0.1*t);
         ball.move(0.1*t);
         if (mode == MODE_EASY)
             app.score++;
      }
      if (duck.c.dist(ball.c) < ball.c.r+duck.c.r)
      {
         duck.move(-0.1*t);
         ball.move(-0.1*t);
         duck.v.x = -duck.v.x;
         duck.v.y = -duck.v.y;
         ball.v.x = -ball.v.x;
         ball.v.y = -ball.v.y;
         duck.move(0.1*t);
         ball.move(0.1*t);
      }
      if (duck.state == FLY && duck.c.y <= 0.07)
      {
         duck.state = WALK;
         duck.c.y = 0.07;
         duck.v.y = 0.00;
         duck.v.x = 0.00;
         duck.c.x = 0.1+roundf((duck.c.x-0.1)*5)/5.0;
      }
      duck.rotat = 0;

      if (ball.c.dist(b1.c) < ball.c.r)
      {
         app.score++;
         b1.newLocation();
      }
      lighthause.visible = app.score > 100;
      fps = 30 + app.score/20*5;

      /* Change music track from time to time...*/
      if (rand() % 10000 == 0)
      {
         app.play_music(rand()%4, 40);
      }

      if (touchdowns - ball.touchdowns == 0)
      {
         app.changeView(mode == MODE_EASY? "game_over_e": "game_over_h");
      }
   }
   int max_fps() { return fps; }
   int mode;
private:
   int fps;
   Window win;
   Ball ball;
   Ball duck;
   BonusArtf b1;
   Item pause;
   Item lighthause;
   Item platforms;
   int touchdowns;
   double time_left;
   int down_x, down_y;
   bool key_left;
   bool key_right;
   bool select;
   bool duck_facing_right;
};
