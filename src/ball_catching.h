#include <iostream>
#include <string>
#include <vector>
#include "geometry.h"
#include "objects.h"
#include "agg_rounded_rect.h"

class BallCatchingGameView : public AppView
{
public:
  BallCatchingGameView(App& app): AppView(app),
      duck(app, 0.02, 1,   1.0, 0.35, 0.03, red, 1, 3),
      c1(app, geometry::P(0.1, 0.3), agg::rgba(1,1,1,0.9), geometry::Vect(-0.002, 0), 0.08),
      c2(app, geometry::P(0.2, 0.2), agg::rgba(1,1,1,0.8), geometry::Vect(0.005, 0), 0.07),
      c3(app, geometry::P(0.4, 0.4), agg::rgba(1,1,1,0.7), geometry::Vect(-0.004, 0), 0.073),
      time_left(0)
   {
      new_game();
   }

   void new_game()
   {
      app.start_timer();
      app.score = 0;
      touchdowns = 10;
      duck.c.x = 0.35;
      duck.c.y = 0.065;
      duck.v.x = 0.0;
      duck.v.y = 0.0;
   }

   void enter()
   {
      new_game();
      app.wait_mode(false);
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      switch (key)
      {
         case 1073741903:
         case agg::key_right:
            if (not flags&KEY_UP)
            {
               duck.c.x += 0.2;
            }
            break;

         case 1073741904:
         case agg::key_left:
            if (not flags&KEY_UP)
            {
               duck.c.x -= 0.2;
            }
            break;

         case 1073741906:
         case agg::key_up:
            //duck.v.y = 0.00;
            break;
      }
      duck.c.x =
         duck.c.x > 1? duck.c.x -1:
         duck.c.x < 0? duck.c.x +1:
         duck.c.x;
   }

   void on_ctrl_change()
   {
      app.force_redraw();
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      down_x = x;
      down_y = y;

      if (geometry::mod(x-0, y-0) < 100)
      {
         duck.c.x -= 0.2;
      }
      if (geometry::mod(x-app.width(), y-0) < 100)
      {
         duck.c.x += 0.2;
      }
      duck.c.x =
         duck.c.x > 1? duck.c.x -1:
         duck.c.x < 0? duck.c.x +1:
         duck.c.x;
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rgba sky(0.1, 0.1, 0.9, 0.2);
      rbase.clear(sky);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      duck.scale = 2.2;

      c1.draw(ras, sl, rbase);
      c2.draw(ras, sl, rbase);
      c3.draw(ras, sl, rbase);

      duck.draw(ras, sl, rbase, duck.v.x < 0);

      for (auto i = balls.begin(); i != balls.end(); i++)
      {
         i->draw(ras, sl, rbase);
      }

      renderer_scanline_type ren_sl(rbase);
      const agg::rgba green(0.3,0.0,0.3,0.9);
      ren_sl.color(green);
      for (int i = 0; i < 5; i++)
      {
        double x  = i*0.2+0.1;
        double xe = i*0.2+0.2;
        agg::rounded_rect rect(app.width()*x, 0, app.width()*xe, app.width()*0.02, app.width()*0.007);
        ras.add_path(rect);
      }
      agg::render_scanlines(ras, sl, ren_sl);
      ras.reset();

      const agg::rgba lblue(0,0.1,0.7,0.3);
      ren_sl.color(lblue);
      agg::ellipse e_bl(0, 0, 100, 100, 30);
      agg::ellipse e_br(app.width(), 0, 100, 100, 30);
      agg::ellipse e_tl(0, app.height(), 100, 100, 30);
      agg::ellipse e_tr(app.width(), app.height(), 100, 100, 30);
      ras.add_path(e_bl);
      ras.add_path(e_br);
      ras.add_path(e_tl);
      ras.add_path(e_tr);
      agg::render_scanlines(ras, sl, ren_sl);

      app.draw_text(0.1*app.width(), 0.9*AR*app.width(), 30.0, lgray, 1.2, std::to_string(app.score).c_str());
      app.draw_text(0.8*app.width(), 0.9*AR*app.width(), 30.0, lgray, 1.2, std::to_string(touchdowns).c_str());
   }

   void update(long elapsed_time)
   {
      double t = 0.25;
      c1.move(t);
      c2.move(t);
      c3.move(t);

      /* Change music track from time to time...*/
      if (rand() % 40 == 0)
      {
         Ball b(app, 0.04, 1, 0.0, 0.95, 0.7, red, 4, 1);
         b.v.x = rand()%5 * -0.018;
         balls.push_back(b);
      }

      for (auto i = balls.begin(); i != balls.end();)
      {
         if (duck.c.dist(i->c) < i->c.r + duck.c.r)
         {
            app.score++;
            i = balls.erase(i);
         }
         else if (i->touchdowns > 0)
         {
            touchdowns--;
            i = balls.erase(i);
         } else {
            i->updateV(t);
            i->move(t);
            i++;
         }
      }

      if (touchdowns == 0)
      {
         app.changeView("game_over");
      }

      /* Change music track from time to time...*/
      if (rand() % 10000 == 0)
      {
         app.play_music(rand()%4, 40);
      }
   }

private:
    Ball duck;
    Cloud c1;
    Cloud c2;
    Cloud c3;
    int touchdowns;
    double time_left;
    int down_x, down_y;
    std::vector<Ball> balls;
};
