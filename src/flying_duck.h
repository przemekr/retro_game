#include <iostream>
#include "geometry.h"
#include "objects.h"

class FlyingGameView : public AppView
{
public:
   FlyingGameView(App& app): AppView(app),
      duck(app, 0.02, 1,   1, 0.5, 0.1, red, 1, 3),
      ball(app, 0.08, 0.5, 3, 0.6, 0.9, lblue, 4, 1),
      c1(app, geometry::P(0.1, 0.3), agg::rgba(1,1,1,0.9), geometry::Vect(-0.002, 0), 0.08),
      c2(app, geometry::P(0.2, 0.2), agg::rgba(1,1,1,0.8), geometry::Vect(0.005, 0), 0.07),
      c3(app, geometry::P(0.4, 0.4), agg::rgba(1,1,1,0.7), geometry::Vect(-0.004, 0), 0.073),
      b1(app, 0.02, 0.2, 0.5, yellow),
      b2(app, 0.02, 0.5, 0.5, yellow),
      b3(app, 0.02, 0.8, 0.5, yellow),
      time_left(0)
   {
      new_game();
   }

   void enter()
   {
      new_game();
      app.wait_mode(false);
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      switch (key)
      {
         case 1073741903:
         case agg::key_right:
            duck.v.x += 0.025;
            break;

         case 1073741904:
         case agg::key_left:
            duck.v.x -= 0.025;
            break;

         case 1073741906:
         case agg::key_up:
            duck.v.y = 0.05;
            break;
      }
   }

   void on_ctrl_change()
   {
      app.force_redraw();
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
   }

   void on_touch_event(float x, float y, float dx, float dy,
         int id, bool down)
   {
      if (id == 0)
      {
         if (!down)
         {
            finger_0_down = false;
         }
         else
         {
            finger_0_down = true;
            finger_0_x = x*app.width();
            finger_0_y = y*AR*app.width();
         }
      }
      if (id == 1)
      {
         if (!down)
         {
            finger_1_down = false;
         }
         else
         {
            finger_1_down = true;
            finger_1_x = x*app.width();
            finger_1_y = y*AR*app.width();
         }
      }

      if (finger_0_down && finger_1_down)
      {
         duck.v.y = 0.05;
      }
   }


   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (geometry::mod(x-0, y-0) < 120)
      {
         duck.v.x -= 0.03;
      }
      if (geometry::mod(x-app.width(), y-0) < 120)
      {
         duck.v.x += 0.03;
      }
      if (y > 120)
      {
         duck.v.y = 0.05;
      }
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rgba sky(0.1, 0.1, 0.9, 0.2);
      rbase.clear(sky);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();

      c1.draw(ras, sl, rbase);
      c2.draw(ras, sl, rbase);
      c3.draw(ras, sl, rbase);

      duck.scale = 1.6;
      duck.draw(ras, sl, rbase, duck.v.x < 0);
      ball.draw(ras, sl, rbase);
      b1.draw(ras, sl, rbase);
      b2.draw(ras, sl, rbase);
      b3.draw(ras, sl, rbase);

      renderer_scanline_type ren_sl(rbase);
      const agg::rgba lgreen(0,1,0,0.6);
      ren_sl.color(lgreen);
      agg::ellipse e_bl(0, 0, 100, 100, 30);
      agg::ellipse e_br(app.width(), 0, 100, 100, 30);
      ras.add_path(e_bl);
      ras.add_path(e_br);
      agg::render_scanlines(ras, sl, ren_sl);

      app.draw_text(0.1*app.width(), 0.9*AR*app.width(), 30.0, lgray, 1.2, std::to_string(app.score).c_str());
      app.draw_text(0.8*app.width(), 0.9*AR*app.width(), 30.0, lgray, 1.2, std::to_string(touchdowns-ball.touchdowns).c_str());
   }

   void update(long elapsed_time)
   {
      double acc_x = 0;
      double acc_y = 0;

      double t1 = geometry::time_to_colision(duck.c, duck.v, 1, 0.75);
      double t2 = geometry::time_to_colision(ball.c, ball.v, 1, 0.75);
      double t3 = geometry::time_to_colision(duck.c, ball.c,
            duck.v, ball.v);
      double t = std::min(0.2, std::min(std::min(t1, t2), t3));

      duck.move(t);
      ball.move(t);
      if (t == t3)
      {
         app.play_sound(1, 1500*geometry::mod(
                  duck.v.x-ball.v.x,
                  duck.v.y-ball.v.y));
         duck.collide(ball);
         duck.move(0.1*t);
         ball.move(0.1*t);
      }
      if (duck.c.dist(ball.c) < ball.c.r+duck.c.r)
      {
         duck.move(-0.1*t);
         ball.move(-0.1*t);
         duck.v.x = -duck.v.x;
         duck.v.y = -duck.v.y;
         ball.v.x = -ball.v.x;
         ball.v.y = -ball.v.y;
         duck.move(0.1*t);
         ball.move(0.1*t);
      }
      duck.updateV(t);
      ball.updateV(t);
      duck.rotatd = 0;
      duck.rotat = atan2(duck.v.y, duck.v.x) + (duck.v.x< 0? -agg::pi: 0);

      c1.move(t);
      c2.move(t);
      c3.move(t);

      if (ball.c.dist(b1.c) < ball.c.r)
      {
         app.score++;
         b1.newLocation();
      }
      if (ball.c.dist(b2.c) < ball.c.r)
      {
         app.score++;
         b2.newLocation();
      }
      if (ball.c.dist(b3.c) < ball.c.r)
      {
         app.score++;
         b3.newLocation();
      }

      /* Change music track from time to time...*/
      if (rand() % 10000 == 0)
      {
         app.play_music(rand()%4, 40);
      }

      if (touchdowns - ball.touchdowns == 0)
      {
         app.changeView("game_over");
      }
   }

   void new_game()
   {
      app.start_timer();
      app.score = 0;
      touchdowns = 10;
      duck.c.x = 0.4;
      duck.c.y = 0.03;
      duck.v.x = 0.0;
      duck.v.y = 0.0;

      ball.v.x = 0.0;
      ball.v.y = 0.0;
      ball.c.x = 0.6;
      ball.c.y = 0.9;
      ball.touchdowns = 0;

      finger_0_down = false;
      finger_1_down = false;
   }

private:
    Ball ball;
    Ball duck;
    Cloud c1;
    Cloud c2;
    Cloud c3;
    double time_left;
    bool finger_0_down;
    int finger_0_x;
    int finger_0_y;

    bool finger_1_down;
    int finger_1_x;
    int finger_1_y;
    int touchdowns;
    BonusArtf b1;
    BonusArtf b2;
    BonusArtf b3;
};
