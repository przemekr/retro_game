#ifndef OBJECTS_H
#define OBJECTS_H

class Item
{
public:
   Item(Window& window, double pos_x, double pos_y, int img_idx):
      win(window), c(pos_x, pos_y), go(c), img(img_idx), go_time(0), wait_time(0),
      visible(true),
      blink_freq(0), blink_time(0), blink_stop(0)
   {
   }
   geometry::P c;
   geometry::P go;
   agg::rgba color;
   Window& win;
   int img;
   double wait_time;
   double go_time;
   bool visible;
   double blink_freq;
   double blink_time;
   double blink_stop;

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
      if (not visible)
         return;

      rbase.blend_from(pixfmt_type(win.app.rbuf_img(img)), 0, win.w(c.x), win.h(c.y));
   }
   void go_to(double n_x, double n_y, double time, double time_wait = 0)
   {
      go.x = n_x;
      go.y = n_y;
      go_time = time;
      wait_time = time_wait;
   }

   void update(double t)
   {
      if (blink_freq)
      {
         blink_time += t;
         if (visible and ((int)(100*blink_time)/(int)(100*blink_freq))%2 == 0)
         {
            visible = false;
         }
         if (not visible and ((int)(100*blink_time)/(int)(100*blink_freq))%2 == 1)
         {
            visible = true;
         }
         if (blink_time > blink_stop)
         {
            blink_freq = 0;
            visible = true;
         }
      }

      wait_time -= t;
      if (wait_time < 0)
         wait_time = 0;
      if (wait_time)
         return;
      if (go_time == 0)
         return;
      if (t > go_time)
         t = go_time;

      double f = t/go_time;
      double f1 = 1-t/go_time;
      c.x = c.x*f1+go.x*f;
      c.y = c.y*f1+go.y*f;
      go_time -= t;
   }

   void blink(double freq, double time)
   {
      blink_time = 0;
      blink_freq = freq;
      blink_stop = time;
   }
};


class BonusArtf
{
public:
   BonusArtf(Window& window,
         double s, double pos_x, double pos_y,
         agg::rgba c, int img, int number_of_images):
      win(window), c(pos_x, pos_y), size(s), color(c), i(0), img_idx(img), img_no(number_of_images)
   {
   }
   geometry::P c;
   double size;
   agg::rgba color;
   Window& win;
   int img_idx;
   int img_no;
   int i;

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
       int img = img_idx + (i++/3)%img_no; 
       int w = win.app.rbuf_img(img).width();
       int h = win.app.rbuf_img(img).height();
       rbase.blend_from(pixfmt_type(win.app.rbuf_img(img)), 0, win.w(c.x)-w/2, win.h(c.y)-h/2);
   }
   void newLocation()
   {
      c.x = 0.1+(double)rand()/RAND_MAX*0.8;
      c.y = 0.70;
   }
};

class Ball
{
public:
   Ball(Window& window, double s, double m, double d,
         double pos_x, double pos_y,
         agg::rgba c, int img, int img_n):
      win(window),
      mass(m), drag(d), c(pos_x, pos_y, s), v(0, 0),
      color(c), img_start(img), img_idx(img), img_no(img_n), state(0), scale(1.0), shift(0,0),
      rotat(0), rotatd(0), touchdowns(0)
      { }
   double mass;
   int state;
   int touchdowns;
   double drag;
   geometry::Circle c;   // location & size
   geometry::Vect   v;   // velocity
   int    img_start;
   int    img_idx;
   int    img_no;
   double scale;
   geometry::P shift;
   double rotat;
   double rotatd;
   agg::rgba color;
   Window& win;

   void updateV(double t)
   {
      double gravity = -0.0098;
      double air_resistance_x = (v.x > 0? -1: 1)* drag*(v.x*v.x);
      double air_resistance_y = (v.y > 0? -1: 1)* drag*(v.y*v.y);
      double acc_x = air_resistance_x;
      double acc_y = gravity + air_resistance_y;

      v.x += t*acc_x;
      v.y += t*acc_y;

   }

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase, bool flip_x = false)
   {
      int image_index = img_start+(img_idx)%img_no;
      int size = win.app.rbuf_img(image_index).width();

      renderer_scanline_type ren_sl(rbase);
      color.opacity(0.0);
      ren_sl.color(color);
      ras.reset();
      agg::ellipse e;
      e.init(win.w(c.x+shift.x), win.h(c.y+shift.y), size/2, size/2, 30);

      agg::trans_affine img_mtx;
      img_mtx *= agg::trans_affine_translation(-size/2, -size/2);
      if (flip_x) img_mtx.flip_x();
      img_mtx *= agg::trans_affine_rotation(rotat);
      img_mtx *= agg::trans_affine_translation(+size/2, +size/2);
      img_mtx *= agg::trans_affine_translation(win.w(c.x)-size/2, win.h(c.y)-size/2);
      img_mtx.invert();

      agg::span_allocator<agg::rgba8> sa;

      typedef agg::span_interpolator_linear<> interpolator_type;
      typedef agg::image_accessor_clip<pixfmt_type> img_source_type;
      typedef agg::span_image_filter_rgba_2x2<img_source_type,
              interpolator_type> span_gen_type;
      interpolator_type interpolator(img_mtx);

      pixfmt_type img_pixf(win.app.rbuf_img(image_index));
      img_source_type img_src(img_pixf, agg::rgba_pre(0, 0.4, 0, 0.5));
      agg::image_filter<agg::image_filter_kaiser> filter;
      span_gen_type sg(img_src, interpolator, filter);

      ras.add_path(e);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);
   }

   void move(double t)
   {
      c.x += t*v.x;
      c.y += t*v.y;

      /* Against walls */
      if (v.x < 0 && c.x-c.r <= 0)
      {
         v.x = -v.x;
         rotatd = 5*v.y;
      }
      if (v.x > 0 && c.x+c.r >= 1)
      {
         v.x = -v.x;
         rotatd = -5*v.y;
      }
      if (v.y < 0 && c.y-c.r <= 0)
      {
         touchdowns++;
         v.y = -v.y;
         rotatd = -5*v.x;
      }
      if (v.y > 0 && c.y+c.r >= 1)
      {
         v.y = -v.y;
         rotatd = -5*v.x;
      }
      rotat += t*rotatd;
   }

   void collide(Ball& other)
   {
      double other_dx = other.v.x;
      double other_dy = other.v.y;
      double mass_fac = mass/other.mass;

      geometry::P this_v(c.x+v.x, c.y+v.y);
      geometry::P point;
      geometry::Line l(c, other.c);
      l.closest_point_on_line(this_v, point);

      v.x = (this_v.x - point.x)/mass_fac;
      v.y = (this_v.y - point.y)/mass_fac;
      other.v.x = (point.x - c.x)*mass_fac;
      other.v.y = (point.y - c.y)*mass_fac;

      geometry::P other_v(other.c.x + other_dx, other.c.y+other_dy);
      l.closest_point_on_line(other_v, point);
      other.v.x += (other_v.x - point.x)*mass_fac;
      other.v.y += (other_v.y - point.y)*mass_fac;
      v.x += (point.x - other.c.x)/mass_fac;
      v.y += (point.y - other.c.y)/mass_fac;

      rotatd = 5*(point.x - other.c.x)/mass_fac;
      other.rotatd = 5*(point.x - c.x)*mass_fac;
   }
};

class Cloud 
{
public:
   Cloud(Window& window, geometry::P center, agg::rgba col, geometry::Vect velocity, double size = 0.1):
      win(window),
      c(center), color(col), v(velocity), s(size)
   {
   }

   void draw(agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase)
   {
      renderer_scanline_type ren_sl(rbase);
      ren_sl.color(color);
      ras.reset();
      agg::ellipse e;
      agg::ellipse e1;
      agg::ellipse e2;
      agg::ellipse e3;
      e.init(win.w(c.x), win.h(c.y), win.s(s), win.s(s*0.3), 20);

      e1.init(win.w(c.x-0.5*s), win.h(c.y), win.s(s*0.25), win.s(s*0.4), 20);
      e2.init(win.w(c.x+0.0*s), win.h(c.y), win.s(s*0.3),  win.s(s*0.5), 20);
      e3.init(win.w(c.x+0.5*s), win.h(c.y), win.s(s*0.25), win.s(s*0.4), 20);
      ras.add_path(e);
      ras.add_path(e1);
      ras.add_path(e2);
      ras.add_path(e3);
      agg::render_scanlines(ras, sl, ren_sl);
   }
   void move(double t)
   {
      c.x += t*v.x;
      c.y += t*v.y;

      /* Against walls */
      if (v.x < 0 && c.x <= 0)
         v.x = -v.x;
      if (v.x > 0 && c.x >= 1)
         v.x = -v.x;
      if (v.y < 0 && c.y <= 0)
         v.y = -v.y;
      if (v.y > 0 && c.y >= 1)
         v.y = -v.y;
   }
private:
   geometry::P c;
   geometry::Vect v;
   agg::rgba color;
   Window& win;
   double s;
};


#endif
