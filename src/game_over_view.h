#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <vector>
#include <time.h>


class GameOverView : public AppView
{
public:
   GameOverView(App& app): AppView(app),
    mode(MODE_EASY),
    win(app, 162, 605, 233, 209),
    pop_up_e(win, 0.1, 0.2, IMG_GAMEO_E),
    pop_up_h(win, 0.1, 0.2, IMG_GAMEO_H)
    {
    }

   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;

      pop_up_e.visible = mode == MODE_EASY;
      pop_up_h.visible = mode == MODE_HARD;

      pop_up_e.draw(ras, sl, rbase);
      pop_up_h.draw(ras, sl, rbase);

      Item d100(win, 0.60, 0.72, IMG_DIG0+app.score/100 %10);
      Item d10 (win, 0.65, 0.72, IMG_DIG0+app.score/10  %10);
      Item d1  (win, 0.70, 0.72, IMG_DIG0+app.score     %10);
      d100.draw(ras, sl, rbase);
      d10.draw(ras, sl, rbase);
      d1.draw(ras, sl, rbase);
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      on_ctrl_change();
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      on_ctrl_change();
   }

   void on_ctrl_change()
   {
      app.changeView("menu");
   }

   void enter()
   {
      std::ifstream file;
      std::ofstream ofile;
      std::string line;

      file.open((app.get_storage_path()+std::string("/easy_top_results")).c_str());
      while (std::getline(file, line))
      {
          top_scores_easy.push_back(std::atoi(line.c_str()));
      }
      file.close();

      file.open((app.get_storage_path()+std::string("/hard_top_results")).c_str());
      while (std::getline(file, line))
      {
          top_scores_hard.push_back(std::atoi(line.c_str()));
      }
      file.close();

      if (mode == MODE_EASY)
          top_scores_easy.push_back(app.score);
      else
          top_scores_hard.push_back(app.score);

      sort(top_scores_easy.rbegin(), top_scores_easy.rend());
      sort(top_scores_hard.rbegin(), top_scores_hard.rend());
      top_scores_easy.resize(std::min((size_t)10, top_scores_easy.size()));
      top_scores_hard.resize(std::min((size_t)10, top_scores_hard.size()));

      ofile.open((app.get_storage_path()+std::string("/easy_top_results")).c_str());
      for (auto i = top_scores_easy.begin(); i != top_scores_easy.end(); i++)
          ofile << *i << std::endl;
      ofile.close();

      ofile.open((app.get_storage_path()+std::string("/hard_top_results")).c_str());
      for (auto i = top_scores_hard.begin(); i != top_scores_hard.end(); i++)
          ofile << *i << std::endl;
      ofile.close();
   }

   int mode;
private:
   int frame;
   Window win;
   Item pop_up_e;
   Item pop_up_h;
   std::vector<int> top_scores_easy;
   std::vector<int> top_scores_hard;
};
