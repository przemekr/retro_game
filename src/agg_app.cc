/*
 * Duck Retro Game ...
 * Copyright 2017 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of Duck Retro Game
 * 
 * Duck Retro Game is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * Duck Retro Game is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Duck Retro Game.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <algorithm> 
#include <stack>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <iostream>
#include <fstream>

#define IMG_BACKGROUND 0
#define IMG_DUCK_E1    1
#define IMG_DUCK_E2    2
#define IMG_DUCK_H1    3
#define IMG_DUCK_H2    4
#define IMG_BALL       5
#define IMG_LEFT       6
#define IMG_RIGHT      7
#define IMG_SELECT     8
#define IMG_TITLE      9
#define IMG_EASY_B     10
#define IMG_HARD_B     11
#define IMG_HSCORE     12
#define IMG_SCORE      13
#define IMG_PAUSE      14
#define IMG_GAMEO_E    15
#define IMG_GAMEO_H    16
#define IMG_COVER      17
#define IMG_COIN1      18
#define IMG_COIN2      19
#define IMG_COIN3      20
#define IMG_COIN4      21
#define IMG_COIN5      22
#define IMG_COIN6      23
#define IMG_DIG0       24
#define IMG_DIG1       25
#define IMG_DIG2       26
#define IMG_DIG3       27
#define IMG_DIG4       28
#define IMG_DIG5       29
#define IMG_DIG6       30
#define IMG_DIG7       31
#define IMG_DIG8       32
#define IMG_DIG9       33
#define IMG_LIGHTH     34
#define IMG_PLATF      35

#include "app_support.h"
#include "walking_duck.h"
#include "menu_view.h"
#include "game_over_view.h"


class the_application: public App
{
public:
   the_application(agg::pix_format_e format, bool flip_y) :
      App(format, flip_y)
   {
      wgame = new WalkingGameView(*this);
      game_over = new GameOverView(*this);
      menu  = new MenuView(*this);
      scores = new ScoreView(*this);
   }

   virtual void changeView(const char* name) 
   {
      std::cout << "change view: " << name << std::endl;
      if (strcmp(name, "menu") == 0)
      {
         view = menu;
      }

      if (strcmp(name, "wgame_e") == 0)
      {
         wgame->mode = MODE_EASY;
         view = wgame;
      }

      if (strcmp(name, "wgame_h") == 0)
      {
         wgame->mode = MODE_HARD;
         view = wgame;
      }

      if (strcmp(name, "game_over_e") == 0)
      {
         view = game_over;
         game_over->mode = MODE_EASY;
      }
      if (strcmp(name, "game_over_h") == 0)
      {
         view = game_over;
         game_over->mode = MODE_HARD;
      }

      if (strcmp(name, "scores") == 0)
         view = scores;

      view->enter();
   };
private:
   WalkingGameView* wgame;
   GameOverView* game_over;
   MenuView* menu;
   ScoreView* scores;
};

int agg_main(int argc, char* argv[])
{
    the_application app(agg::pix_format_bgra32, flip_y);
    app.caption("Retro Duck");
    app.changeView("menu");

    if (false
       || !app.load_img(IMG_BACKGROUND, "master_tv_mh_v1.png")
       || !app.load_img(IMG_DUCK_E1,      "INGAME/Duck1.png")
       || !app.load_img(IMG_DUCK_E2,      "INGAME/Duck2.png")
       || !app.load_img(IMG_DUCK_H1,      "INGAME/Duck3.png")
       || !app.load_img(IMG_DUCK_H2,      "INGAME/Duck4.png")
       || !app.load_img(IMG_BALL,       "INGAME/Ball.png")
       || !app.load_img(IMG_SELECT,     "MAIN/green.png")
       || !app.load_img(IMG_LEFT,       "MAIN/red.png")
       || !app.load_img(IMG_RIGHT,      "MAIN/blue.png")
       || !app.load_img(IMG_TITLE,      "MENU/Logo.png")
       || !app.load_img(IMG_EASY_B,     "MENU/Easy.png")
       || !app.load_img(IMG_HARD_B,     "MENU/Hard.png")
       || !app.load_img(IMG_HSCORE,     "MENU/Score.png")
       || !app.load_img(IMG_SCORE,      "HISCORE/Scoreboard.png")
       || !app.load_img(IMG_PAUSE,      "POPUPS/Paused.png")
       || !app.load_img(IMG_GAMEO_E,    "POPUPS/Over_Easy.png")
       || !app.load_img(IMG_GAMEO_H,    "POPUPS/Over_Hard.png")
       || !app.load_img(IMG_COIN1,      "INGAME/COIN/Coin1.png")
       || !app.load_img(IMG_COIN2,      "INGAME/COIN/Coin2.png")
       || !app.load_img(IMG_COIN3,      "INGAME/COIN/Coin3.png")
       || !app.load_img(IMG_COIN4,      "INGAME/COIN/Coin4.png")
       || !app.load_img(IMG_COIN5,      "INGAME/COIN/Coin5.png")
       || !app.load_img(IMG_COIN6,      "INGAME/COIN/Coin6.png")
       || !app.load_img(IMG_DIG0,       "NUMBERS/0.png")
       || !app.load_img(IMG_DIG1,       "NUMBERS/1.png")
       || !app.load_img(IMG_DIG2,       "NUMBERS/2.png")
       || !app.load_img(IMG_DIG3,       "NUMBERS/3.png")
       || !app.load_img(IMG_DIG4,       "NUMBERS/4.png")
       || !app.load_img(IMG_DIG5,       "NUMBERS/5.png")
       || !app.load_img(IMG_DIG6,       "NUMBERS/6.png")
       || !app.load_img(IMG_DIG7,       "NUMBERS/7.png")
       || !app.load_img(IMG_DIG8,       "NUMBERS/8.png")
       || !app.load_img(IMG_DIG9,       "NUMBERS/9.png")
       || !app.load_img(IMG_COVER,      "MAIN/TVOverlay.png")
       || !app.load_img(IMG_LIGHTH,     "INGAME/Lighthouse.png")
       || !app.load_img(IMG_PLATF,      "INGAME/Platforms.png")
       )
    {
        std::cout << "ERROR, missing gfx file" << std::endl;
        return 1;
    }

    if (app.init(START_W, START_H, WINDOW_FLAGS))
    {
       try {
          return app.run();
       } catch (...) {
          return 0;
       }
    }
    return 1;
}
