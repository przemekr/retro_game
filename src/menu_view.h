class MenuView : public AppView
{
public:
   MenuView(App& app): AppView(app),
      win(app, 160, 596, 248, 209),
      key_left(false),
      key_right(false),
      select(false),
      choice(0),
      logo(win, 0.08, 1.0, IMG_TITLE),
      easy_b(win, 0.10, -0.6, IMG_EASY_B),
      hard_b(win, 0.40, -0.6, IMG_HARD_B),
      score_b(win, 0.70, -0.6, IMG_HSCORE),
      switch_after(0),
      selected(0)
   {
   }

   void enter()
   {
      logo.c.y = 0.9;
      logo.go_to(0.08, 0.4, 0.3,   0.2);
      easy_b.go_to(0.10, 0.0, 0.3, 0.2);
      hard_b.go_to(0.40, 0.0, 0.3, 0.3);
      score_b.go_to(0.70, 0.0, 0.3, 0.4);
      app.wait_mode(false);
      switch_after = 0;
      select = false;
   }
   int max_fps() { return 40; }

   void on_draw()
   {
      double w = app.rbuf_window().width();
      double h = app.rbuf_window().height();

      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      rbase.copy_from(app.rbuf_img(IMG_BACKGROUND));
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      agg::span_allocator<color_type> sa;
      ras.reset();

      if (not key_left)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_LEFT)), 0, 64, 165);

      if (not key_right)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_RIGHT)), 0, 331, 165);

      if (not select)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_SELECT)), 0, 247, 230);

      logo.draw(ras, sl, rbase);
      easy_b.draw(ras, sl, rbase);
      hard_b.draw(ras, sl, rbase);
      score_b.draw(ras, sl, rbase);
      rbase.blend_from(pixfmt_type(win.app.rbuf_img(IMG_COVER)), 0, 0, 0);
   }

   void update(long elapsed_time)
   {
      logo.update(elapsed_time*0.001);
      easy_b.update(elapsed_time*0.001);
      hard_b.update(elapsed_time*0.001);
      score_b.update(elapsed_time*0.001);
      if (switch_after)
      {
         switch_after -= elapsed_time*0.001;
         if (switch_after <= 0)
         {
            switch (selected)
            {
               case 0:
                  app.changeView("wgame_e");
                  break;
               case 1:
                  app.changeView("scores");
                  break;
               case 2:
                  app.changeView("wgame_h");
                  break;
            }
         }
      }
   }

   void on_ctrl_change()
   {
      app.force_redraw();
      if (key_left)
         choice++;
      if (key_right)
         choice--;

      switch (choice%3)
      {
         case 0:
            easy_b.blink(0.5, 1.0);
            break;

         case 1:
            score_b.blink(0.5, 1.0);
            break;

         case 2:
            hard_b.blink(0.5, 1.0);
            break;
      }

      if (select)
      {
         switch (choice%3)
         {
            case 0:
               easy_b.blink(0.2, 1.0);
               break;

            case 1:
               score_b.blink(0.2, 1.0);
               break;

            case 2:
               hard_b.blink(0.2, 1.0);
               break;
            break;
         }
         selected = choice%3;
         switch_after = 1.5;
         logo.go_to(0.08, 1.0,    0.3, 0.2);
         easy_b.go_to(0.10, -0.6, 0.3, 0.2);
         hard_b.go_to(0.40, -0.6, 0.3, 0.3);
         score_b.go_to(0.70, -0.6, 0.3, 0.4);
      }
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      key_left = false;
      key_right = false;
      on_ctrl_change();
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      geometry::P click(x,y);
      if (click.dist(geometry::P(135, 197)) < 68)
         key_left = true;

      if (click.dist(geometry::P(422, 197)) < 68)
         key_right = true;

      if (click.dist(geometry::P(274, 233)) < 35)
         select = true;

      on_ctrl_change();
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      select = false;
      switch (key)
      {
         case 1073741903:
         case agg::key_right:
            if (flags&KEY_UP)
            {
               key_left = false;
               key_right = false;
            }
            else
            {
               key_left = false;
               key_right = true;
            }
            break;

         case 1073741904:
         case agg::key_left:
            if (flags&KEY_UP)
            {
               key_left = false;
               key_right = false;
            }
            else 
            {
               key_left = true;
               key_right = false;
            }
            break;

         case 1073741906:
         case agg::key_up:
            break;

         case 13:
            select = true;
            if (flags&KEY_UP)
            {
               select = false;
            }
            else 
            {
               select = true;
            }
            break;
      }
      on_ctrl_change();
   }

private:
    bool key_left;
    bool key_right;
    bool select;
    unsigned choice;
    int selected;
    Window win;
    Item logo;
    Item easy_b;
    Item hard_b;
    Item score_b;
    double switch_after;
};


class ScoreView: public AppView
{
public:
   ScoreView(App& app): AppView(app),
      win(app, 160, 596, 248, 209),
      scores(win, 0.08, 0.6, IMG_SCORE)
   {
   }

   void enter()
   {
      scores.c.y = 0.5;
      scores.go_to(0.08, -0.2, 1.2, 0.0);
      app.wait_mode(false);

      std::ifstream file;
      std::string line;

      file.open((app.get_storage_path()+std::string("/easy_top_results")).c_str());
      while (std::getline(file, line))
      {
          top_scores_easy.push_back(std::atoi(line.c_str()));
      }
      file.close();

      file.open((app.get_storage_path()+std::string("/hard_top_results")).c_str());
      while (std::getline(file, line))
      {
          top_scores_hard.push_back(std::atoi(line.c_str()));
      }
      file.close();
   }

   void update(long elapsed_time)
   {
      scores.update(elapsed_time*0.001);
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      rbase.copy_from(app.rbuf_img(IMG_BACKGROUND));
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      agg::span_allocator<color_type> sa;
      ras.reset();

      if (not key_left)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_LEFT)), 0, 64, 165);

      if (not key_right)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_RIGHT)), 0, 331, 165);

      if (not select)
         rbase.blend_from(pixfmt_type(app.rbuf_img(IMG_SELECT)), 0, 247, 230);

      scores.draw(ras, sl, rbase);

      // add results for the top 5 scores in easy and hard mode.
      // easy.
      for (auto i = top_scores_easy.begin(); i != top_scores_easy.end(); i++)
      {
          int no = i-top_scores_easy.begin();
          if (no >= 5) break;

          Item d100(win, scores.c.x+0.1+0.00, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i/100 %10);
          Item d10 (win, scores.c.x+0.1+0.05, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i/10  %10);
          Item d1  (win, scores.c.x+0.1+0.10, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i     %10);

          d100.draw(ras, sl, rbase);
          d10.draw(ras, sl, rbase);
          d1.draw(ras, sl, rbase);
      }

      // hard.
      for (auto i = top_scores_hard.begin(); i != top_scores_hard.end(); i++)
      {
          int no = i-top_scores_hard.begin();
          if (no >= 5) break;

          Item d100(win, scores.c.x+0.6+0.00, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i/100 %10);
          Item d10 (win, scores.c.x+0.6+0.05, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i/10  %10);
          Item d1  (win, scores.c.x+0.6+0.10, 0.68+scores.c.y-no*0.14, IMG_DIG0+ *i     %10);

          d100.draw(ras, sl, rbase);
          d10.draw(ras, sl, rbase);
          d1.draw(ras, sl, rbase);
      }
      
      
      rbase.blend_from(pixfmt_type(win.app.rbuf_img(IMG_COVER)), 0, 0, 0);
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      app.changeView("menu");
   }
   void on_ctrl_change()
   {
      app.changeView("menu");
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      key_left = false;
      key_right = false;
      on_ctrl_change();
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      geometry::P click(x,y);
      if (click.dist(geometry::P(144, 294)) < 66)
         key_left = true;

      if (click.dist(geometry::P(409, 294)) < 66)
         key_right = true;

      if (click.dist(geometry::P(279, 269)) < 30)
         select = true;

      on_ctrl_change();
   }

   Window win;
   Item scores;

   bool key_left;
   bool key_right;
   bool select;
   std::vector<int> top_scores_easy;
   std::vector<int> top_scores_hard;
};
