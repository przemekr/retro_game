#!/usr/bin/env python
#encoding: utf-8
VERSION='0.0.1'
APPNAME='retro_duck'

top = '.'

from waflib import Configure, Logs

def options(opt):
        opt.load('compiler_cxx')

def configure(conf):
        conf.load('compiler_cxx')
        conf.check_cfg(
                path='sdl2-config',
                args='--cflags --libs', package = '',
                uselib_store='platform')
        conf.env.PLATFORM_SRC = ['agg-2.5/src/platform/sdl2/agg_platform_support.cpp']
        conf.env.append = ['agg-2.5/src/platform/sdl2/agg_platform_support.cpp']
        conf.check(features='cxx cxxprogram', cxxflags=['-std=c++11'], uselib_store='platform')

def build(bld):
        bld.recurse('src')
