# README #

Retro game. A small callection of simple retro games.


### Build dependency ###

* SDL2
* SDL2_image
* SDL2_audio

### How to build and run for Desktop ####
 ./waf configure
 ./waf
 ./build/src/duck_game


### How to build for Android ###
 cd android
 ndk-build
 ant debug install
