# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
APP_ABI := armeabi-v7a
APP_OPTIM := release
APP_CPPFLAGS += -std=c++11
NDK_TOOLCHAIN_VERSION := 4.9
#APP_STL:=stlport_static
#APP_STL:=c++_shared
APP_STL := gnustl_static
LOCAL_C_INCLUDES += ${ANDROID_NDK}/sources/cxx-stl/gnu-libstdc++/4.8/include
